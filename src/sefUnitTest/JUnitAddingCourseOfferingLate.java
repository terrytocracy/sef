package sefUnitTest;
//Test	no	new	course offerings can be	added 2	weeks prior	to semester commencement
import static org.junit.Assert.*;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
 


import sefUnitTest.DatabaseConn;
public class JUnitAddingCourseOfferingLate {   
	@Test 
	public void test() throws Exception {
		 
		
		DatabaseConn db = new DatabaseConn();
		db.conn();
		
		String courseName = "Test";
		int courseID = 1;
		int assignedLecturerID = 1;
		int adminID = 1;
		int semesterID = 1;
		int courseCount = 0; 
		int courseCount2 = 0; 
		Calendar currentDate = Calendar.getInstance();
		ResultSet resultSet = db.statement.executeQuery("select * from sef.courseofferings");
		
		Statement statement1 = db.conn.createStatement();
		ResultSet resultSet1 = statement1.executeQuery("select semesterStartDate from sef.semesters where semesterID = 1");
		resultSet1.next(); 
		Timestamp semesterStartDate = resultSet1.getTimestamp("semesterStartDate"); 
		
		
		while(resultSet.next()){
			courseCount++;
		}
		
		      //1209600000 = 2 weeks in millis
	    if(semesterStartDate.getTime() - 120960000 < currentDate.getTimeInMillis()) // if semesterStartDate - 2 weeks is < currentDate run
	    {
	    String query = "Insert INTO courseofferings (courseID, assignedLecturerID, semesterID, adminID) VALUES (?,?,?,?)";
		PreparedStatement ps = db.conn.prepareStatement(query);
		ps.setInt(1,courseID );
		ps.setInt(2, assignedLecturerID);
		ps.setInt(3, semesterID);
		ps.setInt(4, adminID);
		ps.execute();
		System.out.println(ps.toString());
	    }
		
		
		ResultSet resultSet2 = db.statement.executeQuery("select * from sef.courseofferings");
	
		while(resultSet2.next()){
			courseCount2++;
			
			System.out.println(courseCount);
		}
		System.out.println(semesterStartDate.getTime() - 2419200000l);
		System.out.println(currentDate.getTimeInMillis());
		System.out.println(courseCount + " " + courseCount2);
		assertEquals(courseCount, courseCount2);
	}
		
			
			
			
			
	}
		
		
		
		
		