package sefUnitTest;
//Test	no	new	courses	can	be	added 4	weeks prior	to semester commencement
import static org.junit.Assert.*;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
 


import sefUnitTest.DatabaseConn;
public class JUnitAddingCourseEarly {   
	@Test 
	public void test() throws Exception {
		 
		
		DatabaseConn db = new DatabaseConn();
		db.conn();
		int courseIsElective = 1;
		int courseLoad = 1;
		int coordinatorID = 1;
		int courseCount = 0; 
		int courseCount2 = 0; 
		Calendar currentDate = Calendar.getInstance();
		String courseName = "Test";
		String coursePrereq1 = "Pre1";
		String coursePrereq2 = "Pre2";
		String courseDetails = "Course 1 Details";
		ResultSet resultSet = db.statement.executeQuery("select * from sef.courses");
		
		Statement statement1 = db.conn.createStatement();
		ResultSet resultSet1 = statement1.executeQuery("select semesterStartDate from sef.semesters where semesterID = 1");
		resultSet1.next(); 
		Timestamp semesterStartDate = resultSet1.getTimestamp("semesterStartDate"); 
		
		
		while(resultSet.next()){
			courseCount++;
		}
		
		      // 2419200000 = 4 weeks in millis
	    if(semesterStartDate.getTime() - 2419200000l > currentDate.getTimeInMillis()) // if semesterStartDate - 4 weeks is < currentDate run
	    {
	    String query = "Insert INTO courses (courseName, courseIsElective, courseLoad, coordinatorID, coursePrereq1, coursePrereq2, courseDetails) VALUES (?,?,?,?,?,?,?)";
		PreparedStatement ps = db.conn.prepareStatement(query);
		
		ps.setString(1, courseName);
		ps.setInt(2, courseIsElective);
		ps.setInt(3, courseLoad);
		ps.setInt(4, coordinatorID);
		ps.setString(5, coursePrereq1);
		ps.setString(6, coursePrereq2);
		ps.setString(7, courseDetails);
		ps.execute();
		System.out.println(ps.toString());
	    }
		
		
		ResultSet resultSet2 = db.statement.executeQuery("select * from sef.courses");
	
		while(resultSet2.next()){
			courseCount2++;
			
			System.out.println(courseCount);
		}
		System.out.println(semesterStartDate.getTime() - 2419200000l);
		System.out.println(currentDate.getTimeInMillis());
		System.out.println(courseCount + " " + courseCount2);
		assertEquals(courseCount+1, courseCount2);
		}
		
			
			
			
			
	}
		
		
		
		
		