package sefUnitTest;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Scanner;
import java.sql.PreparedStatement;
import sefUnitTest.DatabaseConn;

public class Student {
	int studentID;
	String studentName;
	int studentCurrentLoad;
	int studentMaxLoad;
	Boolean studentSpecialPermission;

	public Student() {
		studentID = 0;
		studentName = "No name";
		studentCurrentLoad = 0;
		studentMaxLoad = 0;
		studentSpecialPermission = false;
	}

	public void setID(int id) {
		studentID = id;
	}
	public void displayCourseOfferings(DatabaseConn db) throws SQLException {

		boolean studentSpecialPermission = false;
		Statement studentS = db.conn.createStatement();

		ResultSet studentRS = null, courseOfferingsRS = null;
		try {
			studentRS = studentS.executeQuery("select * from students where studentID = " + studentID);
			studentRS.next();
			studentName = studentRS.getString("studentName");
			studentCurrentLoad = studentRS.getInt("studentCurrentLoad");
			studentMaxLoad = studentRS.getInt("studentMaxLoad");
			studentSpecialPermission = studentRS.getBoolean("studentSpecialPermission");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			courseOfferingsRS = db.statement.executeQuery(
					"select * from courseofferings a, courses b WHERE a.courseID = b.courseID AND semesterID = 1");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Welcome " + studentName + ", your current load is " + studentCurrentLoad
				+ ", your maximum load is " + studentMaxLoad);
		if (studentSpecialPermission == true) {
			System.out.println("You have special permission to overload");
		} else {
			System.out.println("You do not have permission to overload");
		}
		System.out.println("These are the course offerings available for academic year 2016, semester 2:");
		while (courseOfferingsRS.next()) {
			String courseName = courseOfferingsRS.getString("courseName");
			int courseOfferingID = courseOfferingsRS.getInt("courseOfferingID");
			System.out.println(courseOfferingID + ". " + courseName);
		}

	}

	public void enrol(DatabaseConn db, int courseOfferingID) throws SQLException {
		Statement s1 = db.conn.createStatement();
		Statement s2 = db.conn.createStatement();
		PreparedStatement ps = db.conn.prepareStatement(
				"Select * from courseofferings a, courses b where a.courseID = b.courseID AND courseOfferingID = ?");

		ps.setInt(1, courseOfferingID);

		ResultSet rs2 = ps.executeQuery();

		rs2.next();
		int studentEnrollingLoad = rs2.getInt("courseLoad");
		int semesterID = rs2.getInt("semesterID");
		int courseID = rs2.getInt("courseID");
		ResultSet rs = db.statement.executeQuery(
				"select * from enrolments a, courseofferings b WHERE a.courseOfferingID = b.courseOfferingID");

		int dbcourseID = 0;
		boolean enrolCheck = false;

		while (rs.next()) {
			dbcourseID = rs.getInt("courseID");
			if (courseID == dbcourseID) {
				enrolCheck = true;
			}
		}

		if (enrolCheck == false) {
			ResultSet rs1 = s1.executeQuery("select * from exemptions where studentID = 2");
			int exemptionCount = 0;
			int[] exemptions = new int[10];
			while (rs1.next()) {
				exemptions[exemptionCount] = rs1.getInt("courseID");
				exemptionCount++;
			}
			for (int exemption : exemptions) {
				if (exemption == courseID) {
					enrolCheck = true;
				}
			}

			if (studentCurrentLoad + studentEnrollingLoad <= studentMaxLoad || studentSpecialPermission == true) {
				enrolCheck = true;
			} else {
				enrolCheck = false;
			}

		}

		if (enrolCheck == true) {
			String query = "Insert INTO enrolments(studentID,courseOfferingID, semester) VALUES(?,?,?)";
			PreparedStatement ps2 = db.conn.prepareStatement(query);
			ps2.setInt(1, studentID);
			ps2.setInt(2, courseOfferingID);
			ps2.setInt(3, semesterID);
			ps2.execute();
			System.out.println("Enrolled!");

		} else
			System.out.println("You are unable to enrol in this course offering.");

	}
}
