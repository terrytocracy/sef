package sefUnitTest;

import static org.junit.Assert.*;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;



import sefUnitTest.DatabaseConn;
public class JUnitStudentEnrol {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}

	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
	}
    // Tests that student can't enroll in a course that has no offering in the current semester 
	@Test
	public void test() throws Exception {
		//tests that students can enroll in course offering that exists
		
		DatabaseConn db = new DatabaseConn();
		db.conn();
		int studentID = 1;
		int courseOfferingID = 1;
		Timestamp enrolmentDate = java.sql.Timestamp.valueOf(java.time.LocalDateTime.now());
		int semester = 1;
		int enrolCount = 0; /*Counts the number of enrollments */
		ResultSet resultSet = db.statement.executeQuery("select * from sef.courseofferings");
		
		Statement statement2 = db.conn.createStatement();
		Statement statement3 = db.conn.createStatement();
		ResultSet rs = statement2.executeQuery("select * from sef.enrolments");
		
		while(rs.next()){
			enrolCount++;
		}
		while (resultSet.next()) {
		      
		      int dbcourseOfferingID = resultSet.getInt("courseOfferingID");
		     
		      if(dbcourseOfferingID == courseOfferingID){
		    	String query = "Insert INTO enrolments (studentID, courseOfferingID, enrolmentDate, semester) VALUES (?,?,?,?)";
		  		PreparedStatement ps = db.conn.prepareStatement(query);
		  		ps.setInt(1, studentID);
		  		ps.setInt(2, courseOfferingID);
		  		ps.setInt(4, semester);
		  		ps.setTimestamp(3, enrolmentDate);
		  		ps.execute();
		  		System.out.println(ps.toString());
		  		

		      }
		}
		int dbenrolCount = 0;
		ResultSet rs2 = statement3.executeQuery("select * from sef.enrolments");
		
		while(rs2.next()){
			dbenrolCount++;
		}
		System.out.println(dbenrolCount + " " + enrolCount);
		assertEquals(dbenrolCount, enrolCount+1);
		
	}
	//
	@Test
	public void test2() throws Exception {
		// test that users can't enrol in course that doesn't exist. 
		DatabaseConn db = new DatabaseConn();
		db.conn();
		int studentID = 1;
		int courseOfferingID = 100;
		Timestamp enrolmentDate = java.sql.Timestamp.valueOf(java.time.LocalDateTime.now());
		int semester = 1;
		int enrolCount = 0;
		int dbenrolCount = 0;
		ResultSet resultSet = db.statement.executeQuery("select * from sef.courseofferings");
		
		Statement statement2 = db.conn.createStatement();
		Statement statement3 = db.conn.createStatement();
		ResultSet rs = statement2.executeQuery("select * from sef.enrolments");
		
		while(rs.next()){
			enrolCount++;
		}
		
		while (resultSet.next()) {
		      
		      int dbcourseOfferingID = resultSet.getInt("courseOfferingID");
		     
		      if(dbcourseOfferingID == courseOfferingID){
		    	String query = "Insert INTO enrolments (studentID, courseOfferingID, enrolmentDate, semester) VALUES (?,?,?,?)";
		  		PreparedStatement ps = db.conn.prepareStatement(query);
		  		ps.setInt(1, studentID);
		  		ps.setInt(2, courseOfferingID);
		  		ps.setInt(4, semester);
		  		ps.setTimestamp(3, enrolmentDate);
		  		ps.execute();
		  		System.out.println(ps.toString());
		  		
		  		assertEquals(dbcourseOfferingID, courseOfferingID);
		      }
		}	
		ResultSet rs2 = statement3.executeQuery("select * from sef.enrolments");
		
		while(rs2.next()){
			dbenrolCount++;
		}
		System.out.println(dbenrolCount + " " + enrolCount);
		assertEquals(dbenrolCount, enrolCount);
		
	}

}
