package sefUnitTest;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import java.sql.PreparedStatement;

import sefUnitTest.DatabaseConn;


public class Coordinator {
	int coordinatorID;
	String coordinatorName;
	ResultSet resultSet = null;
	

	public Coordinator() {
		

	}

	public void getID() throws Exception{
		DatabaseConn db = open();
		resultSet = db.statement.executeQuery("select coordinatorID from sef.coordinators WHERE coordinatorName");
		//printCoordinators(resultSet);
		addCourse();
		close(db);
	}
	
	public void addCourse() throws Exception{
		DatabaseConn db = open();
		String courseName = "course name";
		String query = "Insert INTO courses (courseName) VALUES (?)";
		PreparedStatement ps = db.conn.prepareStatement(query);
		ps.setString(1, courseName);
		ps.execute();
		resultSet = db.statement.executeQuery("select * from sef.courses");
		printCourses(resultSet);		
	}
	public void coordinatorMenu(Scanner s){
		System.out.println("1. Add course");
		System.out.println("2. Grant exemptions");
		System.out.println("3. Grant special permission");
		System.out.println("Enter your choice: ");
		int choice = s.nextInt();
		switch(choice){
		case 1: 
			try {//TODO INTEGRATE CALLUM CODE
				addCourse();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 2: 
			try {
				grantExemption(s);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 3: 
			try {
				grantSpecialPermission(s);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
			default: 
				System.out.println("You have chosen an invalid option");
		}
	}
	public void grantExemption(Scanner sc) throws Exception{
		DatabaseConn db = open();
		Statement s = db.conn.createStatement();
		ResultSet rs = s.executeQuery("Select * from students");
		while(rs.next()){
			System.out.println(rs.getInt("studentID") + ". " + rs.getString("studentName"));
		}
		System.out.println("Select student to grant exemptions to:");
		int studentID = sc.nextInt();
		ResultSet rs1 = s.executeQuery("select * from courses");
		while(rs1.next()){
			System.out.println(rs1.getInt("courseID")+ ". " + rs1.getString("courseName"));
		}
		System.out.println("Select course to exempt student from");
		int courseID = sc.nextInt();
		PreparedStatement ps = db.conn.prepareStatement("insert into exemptions (studentID,courseID) VALUES (?,?)");
		ps.setInt(1, studentID);
		ps.setInt(2, courseID);
		ps.execute();
		System.out.println("Student granted exemption");
	}
	
	public void grantSpecialPermission(Scanner sc) throws Exception{
		DatabaseConn db = open();
		Statement s = db.conn.createStatement();
		ResultSet rs = s.executeQuery("Select * from students");
		while(rs.next()){
			System.out.println(rs.getInt("studentID") + ". " + rs.getString("studentName"));
		}
		System.out.println("Select student to grant special permission to overload to:");
		int studentID = sc.nextInt();
		
		PreparedStatement ps = db.conn.prepareStatement("Update students set studentSpecialPermission=1 where studentID = ?");
		ps.setInt(1, studentID);
	
		ps.execute();
		System.out.println("Student granted special permission");
		
	}
	
	public void printCourses(ResultSet resultSet) throws SQLException{
		while(resultSet.next()){
			String courseID = resultSet.getString("courseID");
			String courseName = resultSet.getString("courseName");
			System.out.println("Course ID: " + courseID);
			System.out.println("Course name: "+ courseName);
		}
	}
	
	public void printCoordinators(ResultSet resultSet) throws Exception, SQLException {
		DatabaseConn db = open();
		resultSet = db.statement.executeQuery("select * from sef.coordinators");
		 while (resultSet.next()) {
		      // It is possible to get the columns via name
		      // also possible to get the columns via the column number
		      // which starts at 1
		      // e.g. resultSet.getSTring(2);
		      String coordinatorID = resultSet.getString("coordinatorID");
		      String coordinatorName = resultSet.getString("coordinatorName");
		      System.out.println("Coordinator ID: "+ coordinatorID);
		      System.out.println("Coordinator name: "+coordinatorName +"\n");
		      
		    }
	}
	
	private DatabaseConn open() throws Exception{
		DatabaseConn db = new DatabaseConn();
		db.conn();
		return db;
	}
	
	private void close(DatabaseConn db) {
	    try {
	      if (resultSet != null) {
	        resultSet.close();
	      }

	      if (db.statement != null) {
	        db.statement.close();
	      }

	      if (db.conn != null) {
	        db.conn.close();
	      }
	    } catch (Exception e) {

	    }
	  }
}
