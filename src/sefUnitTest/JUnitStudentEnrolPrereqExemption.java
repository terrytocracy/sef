package sefUnitTest;

import static org.junit.Assert.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;
import sefUnitTest.DatabaseConn;

public class JUnitStudentEnrolPrereqExemption {

	@Test
	public void test() throws Exception {
		//try to enrol for courseoffering 4, course 3, have exemptions for course 3
		DatabaseConn db = new DatabaseConn();
		int studentID = 2;
		int courseOfferingID =4;
		try {
			db.conn();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int enrolCount = 0, dbenrolCount = 0;
		Statement s1 = db.conn.createStatement();
		Statement s2 = db.conn.createStatement();
		//get courseid from courseoffering
		ResultSet rs2 = s2.executeQuery("select courseID from courseofferings where courseOfferingID =4");
		rs2.next();
		int courseID = rs2.getInt("courseID");
		
		//check to see if previously enrolled in prereq
		ResultSet rs = db.statement.executeQuery("select * from enrolments a, courseofferings b WHERE a.courseOfferingID = b.courseOfferingID");
		int dbcourseID = 0;
		boolean enrolCheck = false;
		while(rs.next()){
			dbcourseID = rs.getInt("courseID");
			
			if (courseID == dbcourseID ){
				enrolCheck = true;
				
			}
			enrolCount++;
		}
		
		if (enrolCheck == false){
		ResultSet rs1 = s1.executeQuery("select * from exemptions where studentID = 2");
		int exemptionCount = 0;
		int[] exemptions = new int[10];
		while(rs1.next()){
			exemptions[exemptionCount] = rs1.getInt("courseID");
			exemptionCount++;
		}
		for(int exemption : exemptions){
			if(exemption == courseID){
				enrolCheck = true;
			}
		}
		
		
			
		}
		
		if(enrolCheck == true){
			String query = "Insert INTO enrolments(studentID,courseOfferingID) VALUES(?,?)";
			PreparedStatement ps = db.conn.prepareStatement(query);
			ps.setInt(1, studentID);
			ps.setInt(2, courseOfferingID);
			ps.execute();
			
		}
		
		Statement s3 = db.conn.createStatement();
		ResultSet rs3 = s3.executeQuery("select * from enrolments");
		while(rs3.next()){
			dbenrolCount++;
		}
		//dbenrolCount is 1 greater than enrolCount, therefore the enrollment was successful.
		assertEquals(enrolCount+1, dbenrolCount);
	}
	
	@Test
	public void test2() throws Exception {
		//try to enrol for courseoffering 5, course 2, no exemptions
		DatabaseConn db = new DatabaseConn();
		int studentID = 2;
		int courseOfferingID =5;
		try {
			db.conn();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int enrolCount = 0, dbenrolCount = 0;
		Statement s1 = db.conn.createStatement();
		Statement s2 = db.conn.createStatement();
		//get courseid from courseoffering
		ResultSet rs2 = s2.executeQuery("select courseID from courseofferings where courseOfferingID =5");
		rs2.next();
		int courseID = rs2.getInt("courseID");
		
		//check to see if previously enrolled in prereq
		ResultSet rs = db.statement.executeQuery("select * from enrolments a, courseofferings b WHERE a.courseOfferingID = b.courseOfferingID AND studentID = 2");
		int dbcourseID = 0;
		boolean enrolCheck = false;
		while(rs.next()){
			dbcourseID = rs.getInt("courseID");
			
			if (courseID == dbcourseID ){
				enrolCheck = true;
				
			}
			enrolCount++;
		}
		
		if (enrolCheck == false){
			
		//check exemptions 
		ResultSet rs1 = s1.executeQuery("select * from exemptions where studentID = 2");
		int exemptionCount = 0;
		int[] exemptions = new int[10];
		while(rs1.next()){
			exemptions[exemptionCount] = rs1.getInt("courseID");
			exemptionCount++;
		}
		for(int exemption : exemptions){
			if(exemption == courseID){
				enrolCheck = true;
			}
		}
		
		
			
		}
		
		if(enrolCheck == true){
			String query = "Insert INTO enrolments(studentID,courseOfferingID) VALUES(?,?)";
			PreparedStatement ps = db.conn.prepareStatement(query);
			ps.setInt(1, studentID);
			ps.setInt(2, courseOfferingID);
			ps.execute();
		
		}
		
		Statement s3 = db.conn.createStatement();
		ResultSet rs3 = s3.executeQuery("select * from enrolments where studentID = 2");
		while(rs3.next()){
			dbenrolCount++;
		}
		
		assertEquals(enrolCount, dbenrolCount);
	}
	
	@Test
	public void test3() throws Exception {
		//try to enrol for courseoffering 6, course 4, have prereqs for 4
		DatabaseConn db = new DatabaseConn();
		int studentID = 2;
		int courseOfferingID =6;
		try {
			db.conn();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int enrolCount = 0, dbenrolCount = 0;
		Statement s1 = db.conn.createStatement();
		Statement s2 = db.conn.createStatement();
		//get courseid from courseoffering
		ResultSet rs2 = s2.executeQuery("select courseID from courseofferings where courseOfferingID =6");
		rs2.next();
		int courseID = rs2.getInt("courseID");
		
		//check to see if previously enrolled in prereq
		ResultSet rs = db.statement.executeQuery("select * from enrolments a, courseofferings b WHERE a.courseOfferingID = b.courseOfferingID");
		int dbcourseID = 0;
		boolean enrolCheck = false;
		while(rs.next()){
			dbcourseID = rs.getInt("courseID");
			
			if (courseID == dbcourseID ){
				enrolCheck = true;
				
			}
			enrolCount++;
		}
		
		if (enrolCheck == false){
		ResultSet rs1 = s1.executeQuery("select * from exemptions where studentID = 2");
		int exemptionCount = 0;
		int[] exemptions = new int[10];
		while(rs1.next()){
			exemptions[exemptionCount] = rs1.getInt("courseID");
			exemptionCount++;
		}
		for(int exemption : exemptions){
			if(exemption == courseID){
				enrolCheck = true;
			}
		}
		
		
			
		}
		
		if(enrolCheck == true){
			String query = "Insert INTO enrolments(studentID,courseOfferingID) VALUES(?,?)";
			PreparedStatement ps = db.conn.prepareStatement(query);
			ps.setInt(1, studentID);
			ps.setInt(2, courseOfferingID);
			ps.execute();
			
		}
		
		Statement s3 = db.conn.createStatement();
		ResultSet rs3 = s3.executeQuery("select * from enrolments");
		while(rs3.next()){
			dbenrolCount++;
		}
		//dbenrolCount is 1 greater than enrolCount, therefore the enrollment was successful.
		assertEquals(enrolCount+1, dbenrolCount);
	}

}
