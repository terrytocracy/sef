package sefUnitTest;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import sefUnitTest.DatabaseConn;
public class Admin {
	int adminID;
	String adminName;
	private DatabaseConn open() throws Exception{
		DatabaseConn db = new DatabaseConn();
		db.conn();
		return db;
		
	}
	public void assignLecturer() throws Exception{
		DatabaseConn db = open();
		String query = "UPDATE courseofferings set assignedLecturerID = ? where courseOfferingID = ? ";
		PreparedStatement ps = db.conn.prepareStatement(query);
		System.out.println("Enter course offering ID to insert assign lecturer to: ");
		Statement s = db.conn.createStatement();
		ResultSet rs = s.executeQuery("select * from courseofferings a, courses b WHERE b.courseID = a.courseID ORDER BY courseOfferingID");
		while(rs.next()){
			System.out.println(rs.getInt("courseOfferingID") + ". " + rs.getString("courseName"));
		}
		Scanner scanner = new Scanner(System.in);
		int courseOfferingID = scanner.nextInt();
		System.out.println("Enter assigned lecturer's ID: ");
		ResultSet rs1 = s.executeQuery("Select * from lecturers");
		while(rs1.next()){
			System.out.println(rs1.getInt("lecturerID") + ". " + rs1.getString("lecturerName"));
		}
		int lecturerID = scanner.nextInt();
		
		ps.setInt(1, lecturerID);
		ps.setInt(2, courseOfferingID);
		ps.executeUpdate();
		System.out.println("Lecturer assigned.");
		
	}
	
	public void adminMenu(Scanner s){
		System.out.println("1. Add course offering");
		System.out.println("2. Assign Lecturer");
		System.out.println("3. View student performance");
		System.out.println("Enter your choice: ");
		int choice = s.nextInt();
		switch(choice){
		case 1: 
			try {
				addCourseOffering();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 2: 
			try {
				assignLecturer();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 3: 
			//TODO
			System.out.println("Pretend there is functionality here");
			break;
			default: 
				System.out.println("You have chosen an invalid option");
		}
	}
	
	public void addCourseOffering() throws Exception{
		DatabaseConn db = open();
		
		String query = "Insert into courseOfferings (courseID,semesterID) VALUES (?,?)";
		PreparedStatement ps = db.conn.prepareStatement(query);
		System.out.println("Enter course ID to open a new course offering: ");
		Statement s1 = db.conn.createStatement();
		ResultSet rs1 = s1.executeQuery("select * from courses");
		
		while(rs1.next()){
			
			System.out.println(rs1.getInt("courseID") + ". " + rs1.getString("courseName"));
			
		}
		Scanner scanner = new Scanner(System.in);
		int courseID = scanner.nextInt();
		System.out.println("Enter semester ID to open a new course offering in: ");
		Statement s = db.conn.createStatement();
		ResultSet rs = s.executeQuery("Select * from semesters ");
		
		while(rs.next()){
			
			System.out.println(rs.getInt("semesterID") + ". " + rs.getTimestamp("semesterStartDate") + " - " + rs.getTimestamp("semesterEndDate"));
			
		}
		int semesterID = scanner.nextInt();
		ps.setInt(1, courseID);
		ps.setInt(2, semesterID);
		System.out.println(ps.toString());
		ps.execute();
		
		
			
	}
	
	
	
}
