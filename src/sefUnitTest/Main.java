package sefUnitTest;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.Scanner;

public class Main {

	static String wrongUsername(String username, Scanner s) {
		System.out.println("Invalid username entered, please re-enter:");
		username = s.nextLine();
		System.out.println(username);
		return username;
	}

	static int getID(String username, Scanner s) {
		int userID = 0;
		while (true) {
			try {
				userID = Integer.parseInt(username.substring(1));

				break;
			} catch (NumberFormatException e) {
				username = wrongUsername(username, s);
			}
		}
		return userID;
	}

	public static void main(String[] args) throws Exception {

		// assume user is student, display avail courses to enrol in
		DatabaseConn db = new DatabaseConn();
		try {
			db.conn();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Welcome to the course management system");
		while (true) {
			System.out.println("Please enter your username:");
			Scanner s = new Scanner(System.in);
			String username = s.nextLine();
			String name = "";
			PreparedStatement passwordPS = null;
			ResultSet passwordRS = null;
			String password = "";
			String dbPassword = "";
			int userID = 0;
			String role = "";
			Boolean idExists = false;
			while (role == "") {
				switch (username.substring(0, 1)) {
				case "s":
					role = "student";
					userID = getID(username, s);
					System.out.println("Please enter your password:");
					password = s.nextLine();
					passwordPS = db.conn
							.prepareStatement("select studentName,studentPassword from students where studentID = ?");
					passwordPS.setInt(1, userID);
					passwordRS = passwordPS.executeQuery();
					if (!passwordRS.isBeforeFirst()) {
						idExists = false;
						System.out.println("Wrong username/password, try again");
					} else {
						passwordRS.next();
						dbPassword = passwordRS.getString(role + "Password");
						name = passwordRS.getString(role + "Name");
						while (true) {
							if (password.compareTo(dbPassword) != 0) {
								System.out.println("Wrong password, try again");
								password = s.nextLine();
							} else
								break;
						}
						System.out.println("User " + name + " logged in");
						Student student = new Student();
						student.setID(userID);
						student.displayCourseOfferings(db);
						System.out.println("Enter the course offering ID that you would like to enrol in:");
						int courseOfferingID = s.nextInt();
						student.enrol(db, courseOfferingID);
					}
					break;

				case "c":
					role = "coordinator";
					userID = getID(username, s);
					System.out.println("Your id is " + userID);
					System.out.println("Please enter your password:");
					password = s.nextLine();
					passwordPS = db.conn.prepareStatement(
							"select coordinatorName,coordinatorPassword from coordinators where coordinatorID = ?");
					passwordPS.setInt(1, userID);
					passwordRS = passwordPS.executeQuery();
					if (!passwordRS.isBeforeFirst()) {
						idExists = false;
						System.out.println("Wrong username/password, try again");
					} else {
						passwordRS.next();
						dbPassword = passwordRS.getString(role + "Password");
						name = passwordRS.getString(role + "Name");

						while (true) {
							if (password.compareTo(dbPassword) != 0) {
								System.out.println("Wrong password, try again");
								password = s.nextLine();
							} else
								break;
						}
						System.out.println("User " + name + " logged in");
						Coordinator c = new Coordinator();
						c.coordinatorMenu(s);
					}
					break;

				case "l":
					role = "lecturer";
					userID = getID(username, s);
					System.out.println("Your id is " + userID);
					System.out.println("Please enter your password:");
					password = s.nextLine();

					passwordPS = db.conn.prepareStatement(
							"select lecturerName,lecturerPassword from lecturers where lecturerID = ?");
					passwordPS.setInt(1, userID);

					passwordRS = passwordPS.executeQuery();
					if (!passwordRS.isBeforeFirst()) {
						idExists = false;
						System.out.println("Wrong username/password, try again");
					} else {
						passwordRS.next();
						dbPassword = passwordRS.getString(role + "Password");
						name = passwordRS.getString(role + "Name");

						while (true) {
							if (password.compareTo(dbPassword) != 0) {
								System.out.println("Wrong password, try again");
								password = s.nextLine();
							} else
								break;
						}
						System.out.println("User " + name + " logged in");
					}
					break;
				case "a":
					role = "admin";
					userID = getID(username, s);
					System.out.println("Your id is " + userID);
					System.out.println("Please enter your password:");
					password = s.nextLine();
					passwordPS = db.conn
							.prepareStatement("select adminName,adminPassword from admins where adminID = ?");
					passwordPS.setInt(1, userID);
					passwordRS = passwordPS.executeQuery();
					if (!passwordRS.isBeforeFirst()) {
						idExists = false;
						System.out.println("Wrong username/password, try again");
					} else {
						passwordRS.next();
						dbPassword = passwordRS.getString(role + "Password");
						name = passwordRS.getString(role + "Name");

						while (true) {
							if (password.compareTo(dbPassword) != 0) {
								System.out.println("Wrong password, try again");
								password = s.nextLine();
							} else
								break;
						}
						System.out.println("User " + name + " logged in");
						Admin a = new Admin();
						a.adminMenu(s);
					}
					break;
				
				default:
					username = wrongUsername(username, s);
				}

			}
		}
		// int studentID = 1; // assuming student 1 is accessing the app

	}

}
