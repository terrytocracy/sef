package sefUnitTest;

import static org.junit.Assert.*;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDateTime;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import sefUnitTest.Student;
import sefUnitTest.DatabaseConn;

public class JUnitStudentEnrolSpecialPermissionTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception {
		//test that student can enrol if current load + load to be enrolled <= max load
		DatabaseConn db = new DatabaseConn();
		db.conn();
		int enrolCount=0, dbenrolCount = 0;
		Statement s1 = db.conn.createStatement();
		Statement s2 = db.conn.createStatement();
		ResultSet rs = s1.executeQuery("select * from enrolments");
		while(rs.next()){
			enrolCount++;
			
		}
		ResultSet rs2 = db.statement.executeQuery("Select * from students where studentID=1");
		rs2.next();
		int studentCurrentLoad = rs2.getInt("studentCurrentLoad");
		int studentMaxLoad = rs2.getInt("studentMaxLoad");
		int studentPermission = rs2.getInt("studentSpecialPermission");
		int studentEnrollingLoad= 4;
		if(studentCurrentLoad+studentEnrollingLoad<=studentMaxLoad || studentPermission == 1){
			
			String query = "INSERT INTO enrolments (studentID,courseOfferingID) Values (1,1)";
			PreparedStatement ps = db.conn.prepareStatement(query);
			ps.execute();
		} 
		ResultSet rs3 = s2.executeQuery("select * from enrolments");
		while(rs3.next()){
			dbenrolCount++;
		}
		System.out.println(enrolCount + " " + dbenrolCount);
		assertEquals(enrolCount+1,dbenrolCount);
	}
	
	@Test
	public void test2() throws Exception {
		//test that student can enrol if current load + load to be enrolled > max load but he has special permission
		DatabaseConn db = new DatabaseConn();
		db.conn();
		int enrolCount=0, dbenrolCount = 0;
		Statement s2 = db.conn.createStatement();
		Statement s3 = db.conn.createStatement();
		ResultSet rs = s2.executeQuery("select * from enrolments");
		while(rs.next()){
			enrolCount++;
			
		}
		ResultSet rs2 = db.statement.executeQuery("Select * from students where studentID=1");
		rs2.next();
		int studentCurrentLoad = rs2.getInt("studentCurrentLoad");
		int studentMaxLoad = rs2.getInt("studentMaxLoad");
		int studentPermission = rs2.getInt("studentSpecialPermission");
		int studentEnrollingLoad= 17;
		if(studentCurrentLoad+studentEnrollingLoad<=studentMaxLoad|| studentPermission == 1){
			
			String query = "INSERT INTO enrolments (studentID,courseOfferingID) Values (1,1)";
			PreparedStatement ps = db.conn.prepareStatement(query);
			ps.execute();
		} 
		ResultSet rs3 = s3.executeQuery("select * from enrolments");
		while(rs3.next()){
			dbenrolCount++;
		}
		System.out.println(enrolCount + " " + dbenrolCount);
		assertEquals(enrolCount+1,dbenrolCount);
	}
	@Test
	public void test3() throws Exception {
		//test that student can't enrol if current load + load to be enrolled > max load and student doesn't have special permission
		DatabaseConn db = new DatabaseConn();
		db.conn();
		int enrolCount=0, dbenrolCount = 0;
		Statement s2 = db.conn.createStatement();
		Statement s3 = db.conn.createStatement();
		ResultSet rs = s2.executeQuery("select * from enrolments");
		while(rs.next()){
			enrolCount++;
			
		}
		ResultSet rs2 = db.statement.executeQuery("Select * from students where studentID=2");
		rs2.next();
		int studentCurrentLoad = rs2.getInt("studentCurrentLoad");
		int studentMaxLoad = rs2.getInt("studentMaxLoad");
		int studentPermission = rs2.getInt("studentSpecialPermission");
		int studentEnrollingLoad= 17;
		if(studentCurrentLoad+studentEnrollingLoad<=studentMaxLoad|| studentPermission == 1){
			
			String query = "INSERT INTO enrolments (studentID,courseOfferingID) Values (1,1)";
			PreparedStatement ps = db.conn.prepareStatement(query);
			ps.execute();
		} 
		ResultSet rs3 = s3.executeQuery("select * from enrolments");
		while(rs3.next()){
			dbenrolCount++;
		}
		System.out.println(enrolCount + " " + dbenrolCount);
		assertEquals(enrolCount,dbenrolCount);
	}
	

}
